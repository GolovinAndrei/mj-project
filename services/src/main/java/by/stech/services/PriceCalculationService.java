package by.stech.services;

import by.stech.model.Order;

public interface PriceCalculationService {

public double repairHours (Order order);
public double costOfWork(Order order);
public double totalPrice(Order order);
}
