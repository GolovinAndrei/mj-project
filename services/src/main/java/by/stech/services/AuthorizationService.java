
package by.stech.services;

import by.stech.model.Customer;
import by.stech.model.Employee;

import java.io.IOException;
import java.sql.SQLException;

public interface AuthorizationService {

    public Customer authCustomer(String login, String password) throws SQLException, IOException;

    public Employee authEmployee(String login, String password) throws SQLException, IOException;

    public boolean isCustomer(String login) throws SQLException, IOException;

    public boolean isEmployee(String login) throws SQLException, IOException;

}

