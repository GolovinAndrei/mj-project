package by.stech.services.impl;


import by.stech.db.repository.OrderRepository;
import by.stech.model.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class OrderRepositoryService {

    @Autowired
    private OrderRepository orderRepository;


    public void updateById(Long Id) {
    }

    public List<Order> orderList() {
        return orderRepository.findAll();
    }

    public void add(Order order) {
        orderRepository.save(order);
        return;
    }

    public Order findById(Long id) {

        return orderRepository.findById(id).orElseThrow(IllegalArgumentException::new);
    }

    public void delete(Long id) {
        orderRepository.deleteById(id);
    }
}


