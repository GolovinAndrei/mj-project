package by.stech.services.impl;

import by.stech.db.repository.VehicleRepository;
import by.stech.model.Customer;
import by.stech.model.Vehicle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class VehicleRepositoryService {

    @Autowired
    private VehicleRepository vehicleRepository;


    public List<Vehicle> vehicleList() {
        return vehicleRepository.findAll();
    }

    public void addVehicle(Vehicle vehicle) {
        vehicleRepository.save(vehicle);
        return;
    }

    public Vehicle findById(Long id) {
        return vehicleRepository.findById(id).orElseThrow(IllegalArgumentException::new);
    }

    public void deleteVehicle (Long id) {
        vehicleRepository.deleteById(id);
    }


    public List <Vehicle> findAllByCustomer(Customer customer) {
        return vehicleRepository.findAllByCustomer(customer);
    }


}

