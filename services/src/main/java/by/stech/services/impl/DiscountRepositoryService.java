package by.stech.services.impl;

import by.stech.db.repository.DiscountRepository;
import by.stech.model.Discount;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DiscountRepositoryService {

    @Autowired
    private DiscountRepository discountRepository;

    public List<Discount> discountList() {
        return discountRepository.findAll();
    }

    public void addDiscount(Discount discount) {
        discountRepository.save(discount);
        return;
    }

    public Discount discountById (Long id) {
        return discountRepository.findById(id).orElse(null);
    }



    public void deleteDiscount(Long id) {
        discountRepository.deleteById(id);

    }
}


