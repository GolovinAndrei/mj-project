package by.stech.services.impl;

import by.stech.model.Mechanic;
import by.stech.model.Order;
import org.springframework.stereotype.Service;


@Service
public class PriceCalculationServiceImpl {


    public double costOfWork(Order order) {
        double hoursInWork = (double) ((parseTimeToMin(order.getFinishRepair()) - parseTimeToMin(order.getTakeCar())) / 60.0);
        double res = 0;
        for (Mechanic mec : order.getMechanics()) {
            res += mec.getSalaryForHour() * hoursInWork;
        }
        return res;
    }

    public double totalPrice(Order order) {
        double discountValue = (double) (order.getVehicle().getCustomer().getDiscount().getDiscount() / 100.00);
        double totalPrice = costOfWork(order) + order.getFault().getRepairCost();
        return (totalPrice - totalPrice * discountValue);

    }

    public int parseTimeToMin(String dateAndTime) {
        String[] devDateAndTime = dateAndTime.split("T");
        String[] timeParts = devDateAndTime[1].split(":");
        return (60 * Integer.parseInt(timeParts[0]) + Integer.parseInt(timeParts[1]));
    }


}
