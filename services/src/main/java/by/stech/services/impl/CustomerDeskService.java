package by.stech.services.impl;

import by.stech.model.Order;
import by.stech.model.Vehicle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CustomerDeskService {

    @Autowired
    VehicleRepositoryService vehicleRepositoryService;

    @Autowired
    CustomerRepositoryService customerRepositoryService;

    @Autowired
    OrderRepositoryService orderRepositoryService;

    public List<Vehicle> customersVehicles() {
        return vehicleRepositoryService.findAllByCustomer(customerRepositoryService.findCustomerByLogin(extractLogin()));

    }

    public String extractLogin() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        return auth.getName();
    }

    public  List<Order> customersOrdersInWork (){
        List <Order> customersOrdersInWork = new ArrayList<>();
        for (Vehicle vh:customersVehicles()){
            for (Order order:vh.getOrders()){
                if (order.getOrdersStatus().equals("new")){
                    customersOrdersInWork.add(order);
                }
            }
        }
        return customersOrdersInWork;
    }

}
