package by.stech.services.impl;

import by.stech.db.connection.CustomerDao;
import by.stech.db.connection.EmployeeDao;
import by.stech.db.connection.impl.CustomerDaoImpl;
import by.stech.db.connection.impl.EmployeeDaoImpl;
import by.stech.db.repository.CustomerRepository;
import by.stech.db.repository.MechanicRepository;
import by.stech.model.Customer;
import by.stech.model.Employee;
import by.stech.services.AuthorizationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.sql.SQLException;


public class AuthorizationServiceImpl implements AuthorizationService {

    private static AuthorizationService instance;

    public static AuthorizationService getInstance() {
        if (instance == null) {
            synchronized (AuthorizationService.class) {
                if (instance == null) {
                    instance = new AuthorizationServiceImpl();
                }
            }
        }
        return instance;
    }


    private final CustomerDao customerDao = CustomerDaoImpl.getInstance();
    private final EmployeeDao employeeDao = EmployeeDaoImpl.getInstance();

    @Override
    public Customer authCustomer(String login, String password) throws SQLException, IOException {
        final Customer customer = customerDao.getByLogin(login);
        if (password.equals(customer.getPassword())) {
            return customer;

        } else {
            return null;
        }
    }


    @Override
    public Employee authEmployee(String login, String password) throws SQLException, IOException {
        final Employee employee = employeeDao.getByLogin(login);
        if (password.equals(employee.getPassword())) {
            return employee;
        } else {
            return null;
        }

    }

    @Override
    public boolean isCustomer(String login) throws SQLException, IOException {

        for (String s : customerDao.getAllLogins()) {
            if (login.equalsIgnoreCase(s)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean isEmployee(String login) throws SQLException, IOException {
        for (String s : employeeDao.getAllLogins()) {
            if (login.equalsIgnoreCase(s)) {
                return true;
            }
        }
        return false;
    }


}

