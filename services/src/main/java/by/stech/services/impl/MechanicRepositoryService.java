package by.stech.services.impl;

import by.stech.db.repository.MechanicRepository;
import by.stech.model.Mechanic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class MechanicRepositoryService {

    @Autowired
   private MechanicRepository mechanicRepository;

   public List <Mechanic> mechanicList (){
       return mechanicRepository.findAll();
   }

   public void add (Mechanic mechanic){
       mechanicRepository.save(mechanic);
       return;
   }

   public void delete (Long id){
       mechanicRepository.deleteById(id);
   }

   public Mechanic mechanicByLogin (String login){
       return mechanicRepository.findAllByLogin(login).orElse(null);
   }
}
