package by.stech.services.impl;


import by.stech.db.repository.FaultRepository;
import by.stech.model.Fault;
import by.stech.model.Mechanic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FaultRepositoryService {

    @Autowired
    private FaultRepository faultRepository;

    public Fault faultById (Long id){
        return faultRepository.findById(id).orElse(null);
    }

    public void updateById(Long Id) {

    }

    public List<Fault> faultList() {
        return faultRepository.findAll();
    }

    public void addFault(Fault fault) {
        faultRepository.save(fault);
        return;
    }

    public void deleteFault (Long id) {
        faultRepository.deleteById(id);
    }
}


