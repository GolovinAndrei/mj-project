package by.stech.services.impl;


import by.stech.model.Mechanic;
import by.stech.model.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TimeTableService {

    @Autowired
    private OrderRepositoryService orderRepositoryService;

    @Autowired
    private MechanicRepositoryService mechanicRepositoryService;

    public List<Order> ordersForMechanic() {
        final List<Order> orders = orderRepositoryService.orderList();
        final List<Mechanic> mechanics = mechanicRepositoryService.mechanicList();
        Long mechanicId = 0L;
        for (Mechanic mec : mechanics) {
            if (extractLogin().equals(mec.getLogin())) {
                mechanicId = mec.getId();
            }
        }
        List<Order> ordersForMechanic = new ArrayList<>();
        for (Order order : orders) {
            for (Mechanic mechanic : order.getMechanics()) {
                if (mechanic.getId().equals(mechanicId)) {
                    ordersForMechanic.add(order);
                }
            }
        }
        return ordersForMechanic;
    }

    public String extractLogin() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        return auth.getName();
    }

    public List<Order> activeOrdersForMechanic(List<Order> orders) {
        List<Order> activeOrders = new ArrayList<>();
        for (Order order : orders) {
            if (order.getOrdersStatus().equalsIgnoreCase("new")) {
                activeOrders.add(order);
            }
        }
        return activeOrders;
    }
}
