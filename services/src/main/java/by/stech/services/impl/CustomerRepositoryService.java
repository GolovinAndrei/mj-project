package by.stech.services.impl;

import by.stech.db.repository.CustomerRepository;
import by.stech.db.repository.DiscountRepository;
import by.stech.model.Customer;
import by.stech.model.Discount;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class CustomerRepositoryService {

    @Autowired
    private CustomerRepository customerRepository;

    public void updateById(Long Id) {
    }

    public List<Customer> customerList() {
        return customerRepository.findAll();
    }

    public void addCustomer(Customer customer) {
        customerRepository.save(customer);
        return;
    }

    public void deleteCustomer(Long id) {
        customerRepository.deleteById(id);
    }


    public Customer findCustomerByLogin(String login) {
        return customerRepository.findAllByLogin(login).orElseThrow(IllegalArgumentException::new);
    }

}

