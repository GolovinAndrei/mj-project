package by.stech.services.security;

import by.stech.db.repository.CustomerRepository;
import by.stech.db.repository.EmployeeRepository;
import by.stech.model.Customer;
import by.stech.model.Employee;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service("authService")
public class AuthenticationService implements UserDetailsService {

   @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private EmployeeRepository employeeRepository;


    @Override
    @Transactional
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {

        Employee authEmployee = employeeRepository.findAllByLogin(login).orElse(null);
        Customer authCustomer = customerRepository.findAllByLogin(login).orElse(null);
        if (authEmployee != null) {
            if (login.equalsIgnoreCase("admin")){
                return new User(authEmployee.getLogin(), "{noop}"+authEmployee.getPassword(), grantedAuthorities(1));
            }
            else {return new User(authEmployee.getLogin(), "{noop}"+authEmployee.getPassword(), grantedAuthorities(2));}

        } else
            if (authCustomer != null) {
                 return new User(authCustomer.getLogin(), "{noop}"+authCustomer.getPassword(), grantedAuthorities (3));
                } else {
                throw new UsernameNotFoundException("User not found!");
            }
        }

    private List<GrantedAuthority> grantedAuthorities (int pos){
        List<GrantedAuthority> authorities = new ArrayList<>();
        switch (pos) {
            case 1:
                authorities.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
                break;
            case 2:
                authorities.add(new SimpleGrantedAuthority("ROLE_MECHANIC"));
                break;
            case 3:
                authorities.add(new SimpleGrantedAuthority("ROLE_CUSTOMER"));
                break;
        }
        return authorities;
}


}

