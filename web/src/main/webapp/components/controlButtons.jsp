<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Sections</title>
    <link href="../WEB-INF/css/style.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<div>

    <div>
        <form>
            <input class="control-btn" type="button" value="Customers" onclick="window.location.href='customerPanel.form'"/>
        </form>
    </div>

    <div>
        <form>
            <input class="control-btn" type="button" value="Employees" onclick="window.location.href='employeePanel.form'"/>
        </form>
    </div>

    <div>
        <form>
            <input class="control-btn" type="button" value="Orders" onclick="window.location.href='orderPanel.form'"/>
        </form>
    </div>

    <div >
        <form>
            <input class="control-btn" type="button" value="Cars" onclick="window.location.href='vehiclePanel.form'"/>
        </form>
    </div>

    <div >
        <form>
            <input class="control-btn" type="button" value="Faults" onclick="window.location.href='faultPanel.form'"/>
        </form>
    </div>
    <div>
        <form>
            <input class="control-btn" type="button" value="Discounts" onclick="window.location.href='discountPanel.form'"/>
        </form>
    </div>

</div>
</body>
</html>
