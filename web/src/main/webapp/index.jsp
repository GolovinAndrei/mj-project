<%@ taglib prefix="s" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<style>
    <%@include file="/css/style.css" %>
</style>
<c:url var="loginUrl" value="/login"/>
<c:set var="context" value="${pageContext.request.contextPath}"/>
<html>
<head>
    <title>Index page</title>
    <style>
        body {
            background-image: url("http://pluspng.com/img-png/white-bmw-m2-coupe-front-view-car-png-image-1683.png");
            background-position: center center;
            background-attachment: fixed;
            background-size: cover;
        }
    </style>
</head>
<body>
<header>
    <div>
        <h1 class="index-page-title"> Welcome to Car Service</h1>
    </div>
</header>
<div>

    <s:authorize access="isAuthenticated()">

        <s:authorize access="hasRole('ROLE_ADMIN')">
            <h2>Administration panels</h2>

            <p> Yoa are: <s:authentication property="principal.username"/></p>

            <input class="button-index-page" type="button" value="Customers"
                   onclick="window.location.href='customerPanel.form'"/>  <br/>
            <input class="button-index-page" type="button" value="Employees"
                   onclick="window.location.href='employeePanel.form'"/>  <br/>
            <input class="button-index-page" type="button" value="Orders"
                   onclick="window.location.href='orderPanel.form'"/>  <br/>
            <input class="button-index-page" type="button" value="Cars"
                   onclick="window.location.href='vehiclePanel.form'"/>  <br/>
            <input class="button-index-page" type="button" value="Faults"
                   onclick="window.location.href='faultPanel.form'"/>  <br/>
            <input class="button-index-page" type="button" value="Discounts"
                   onclick="window.location.href='discountPanel.form'"/>  <br/>

        </s:authorize>
        <s:authorize access="hasRole('ROLE_CUSTOMER')">
            <h2>Customers info</h2>
            <input class="button-index-page" type="button" value="My Orders in progress"
                   onclick="window.location.href='customersDesk.form'"/><br/>
            <input class="button-index-page" type="button" value="My Cars"
                   onclick="window.location.href='customersHistory.form'"/><br/>
        </s:authorize>
        <s:authorize access="hasRole('ROLE_MECHANIC')">
            <h2>Employee menu</h2>
            <input class="button-index-page" type="button" value="My timetable"
                   onclick="window.location.href='timetableEmployee.form'"/><br/>
            <input class="button-index-page" type="button" value="New order"
                   onclick="window.location.href='newOrder.form'"/>
        </s:authorize>

        <br/>
        <input class="logout-button" type="button" value="Logout" onclick="window.location.href='${context}/login.jspx'"/>
       <%-- <a href="${context}/login.jspx">Logout</a>--%>

    </s:authorize>
</div>
</body>
<%@include file="/components/footer.jsp" %>
</html>
