package by.stech.web;

import by.stech.model.Fault;
import by.stech.model.Order;
import by.stech.services.impl.FaultRepositoryService;
import by.stech.services.impl.OrderRepositoryService;
import by.stech.services.impl.PriceCalculationServiceImpl;
import by.stech.services.impl.TimeTableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;


@Controller
@Transactional
@RequestMapping(path = "/timetableEmployee")
public class TimetableController {

    @Autowired
    private OrderRepositoryService orderRepositoryService;

    @Autowired
    private TimeTableService timeTableService;

    @Autowired
    private FaultRepositoryService faultRepositoryService;

    @Autowired
    private Container container;

    @Autowired
    private Order orderInWork;

    @Autowired
    private PriceCalculationServiceImpl priceCalculationService;


    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView get(ModelAndView modelAndView) {

        return fillOrderInWork(modelAndView);
    }

    @RequestMapping(method = RequestMethod.POST, params = {"orderIdForWork"})
    public ModelAndView startWork(@ModelAttribute("orderInWork") Order orderInWork, ModelAndView modelAndView,
                                  @RequestParam(value = "orderIdForWork") Long orderIdForWork) {
        orderInWork = orderRepositoryService.findById(orderIdForWork);
        orderInWork.setTakeCar(timeInString());
        orderRepositoryService.add(orderInWork);

        return fillOrderInWork(modelAndView, orderInWork, container);
    }

    @RequestMapping(method = RequestMethod.POST, params = "endWork")
    public ModelAndView endWork(@ModelAttribute("container") Container container, ModelAndView modelAndView,
                                @RequestParam(value = "endWork") Long endWork) {

        Order orderInWork = orderRepositoryService.findById(endWork);

        orderInWork.setFault(faultRepositoryService.faultById((container.getFaultId())));
        orderInWork.setFinishRepair(timeInString());
        orderInWork.setOrdersStatus("complete");
        orderInWork.setTotalPrice(priceCalculationService.totalPrice(orderInWork));
        orderRepositoryService.add(orderInWork);
        return fillOrderInWork(modelAndView);
    }

    @ModelAttribute("faultsForOrderInWork")
    public List<Fault> faultList() {
        return faultRepositoryService.faultList();
    }


    private String timeInString() {
        LocalDateTime currentDateTime = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ISO_DATE_TIME;
        return currentDateTime.format(formatter);
    }


    public ModelAndView fillOrderInWork(ModelAndView modelAndView) {
        return fillOrderInWork(modelAndView, orderInWork, container);
    }

    public ModelAndView fillOrderInWork(ModelAndView modelAndView, Container container) {
        return fillOrderInWork(modelAndView, orderInWork, container);
    }

    public ModelAndView fillOrderInWork(ModelAndView modelAndView, Order orderInWork, Container container) {
        List<Order> ordersForMechanic = timeTableService.ordersForMechanic();
        modelAndView.addObject("ordersForMechanic", timeTableService.activeOrdersForMechanic(ordersForMechanic));
        modelAndView.addObject("container", container);
        modelAndView.addObject("orderInWork", orderInWork);
        return modelAndView;
    }

}



















