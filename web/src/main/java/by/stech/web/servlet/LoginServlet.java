/*

package by.stech.web.servlet;

import by.stech.model.Customer;
import by.stech.model.Employee;
import by.stech.services.AuthorizationService;
import by.stech.services.impl.AuthorizationServiceImpl;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;


@WebServlet(name = "LoginServlet", urlPatterns = "/login")
public class LoginServlet extends HttpServlet {

    private static final String LOGIN_PAGE_PATH = "/login.jsp";
    private static final String PATH_FOR_CUSTOMER = "/customersInfo.jsp";
    private static final String PATH_FOR_ADMIN = "/customers";
    private static final String PATH_FOR_EMPLOYEE = "/timetable.jsp";

    private final AuthorizationService authorizationService = AuthorizationServiceImpl.getInstance();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

      String login = request.getParameter("login");
     String passwordFromUser = request.getParameter("password");
        Map<String, String> messages = new HashMap<String, String>();
        if (login == null || login.isEmpty()) {
            messages.put("email", "Please enter login");
        }

        if (passwordFromUser == null || passwordFromUser.isEmpty()) {
            messages.put("password", "Please enter password");
        }
        if (messages.isEmpty()) {
            HttpSession session = request.getSession(true);
            try {
                if (authorizationService.isCustomer(login)) {


                    Customer customer = authorizationService.authCustomer(login, passwordFromUser);
                    if (customer == null) {
                        messages.put("password", "Incorrect password, please try again");
                        request.setAttribute("messages", messages);
                        response.sendRedirect(LOGIN_PAGE_PATH);
                    }
                    session.setAttribute("user", "customer");
                    session.setAttribute("name", customer.getName());
                    session.setAttribute("surname", customer.getSurname());
                    request.getRequestDispatcher(PATH_FOR_CUSTOMER).forward(request, response);
                    return;
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }

            try {
               if (authorizationService.isEmployee(login)) {

                    Employee employee = authorizationService.authEmployee(login, passwordFromUser);
                    if (employee == null) {
                        messages.put("password", "Incorrect password, please try again");
                        request.setAttribute("messages", messages);
                        response.sendRedirect(LOGIN_PAGE_PATH);
                    }
                    session.setAttribute("user", employee);
                    session.setAttribute("name", employee.getName());
                    session.setAttribute("surname", employee.getSurname());
                    if (login.equals("admin")){

                        //request.getRequestDispatcher( "/customers").forward(request, response);
                        response.sendRedirect(request.getContextPath()+PATH_FOR_ADMIN);
                    }else{

                            request.getRequestDispatcher(PATH_FOR_EMPLOYEE).forward(request, response);
                        }

                    return;
                } else {
                    messages.put("login", "There is no such login or it is entered incorrectly, please try again");
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }

 */
/*
 request.setAttribute("messages", messages);
            request.getRequestDispatcher("/login.jsp").forward(request, response);
*//*

        }
        request.setAttribute("messages", messages);
        response.sendRedirect(LOGIN_PAGE_PATH);


    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws
            ServletException, IOException {

       request.getRequestDispatcher(LOGIN_PAGE_PATH).forward(request, response);
    }


}
*/
