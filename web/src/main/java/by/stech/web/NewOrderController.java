package by.stech.web;

import by.stech.model.Mechanic;
import by.stech.model.Order;
import by.stech.model.Vehicle;
import by.stech.services.impl.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;


import java.util.ArrayList;
import java.util.List;

@Transactional
@Controller
@RequestMapping(path = "/newOrder")
public class NewOrderController {

    @Autowired
    private OrderRepositoryService orderRepositoryService;

    @Autowired
    private MechanicRepositoryService mechanicRepositoryService;

    @Autowired
    private VehicleRepositoryService vehicleRepositoryService;

    @Autowired
    private TimeTableService timeTableService;

    @Autowired
    Container container;

    private Order order = new Order();

    @RequestMapping(method= RequestMethod.GET)
    public ModelAndView get (ModelAndView modelAndView){
        modelAndView.addObject("mechanics", getAuthMechanics());
        modelAndView.addObject("container", container);
        return modelAndView;

    }

    @RequestMapping (method=RequestMethod.POST)
    public String add (ModelAndView modelAndView, @ModelAttribute("container") Container container){
        order.setMechanics(getAuthMechanics());
        order.setVehicle(vehicleRepositoryService.findById(container.getVehicleId()));
        order.setOrdersStatus("new");
        orderRepositoryService.add(order);
        return "timetableEmployee";
    }

    @ModelAttribute ("vehiclesForNewOrder")
    public List<Vehicle> vehicleList (){
        return vehicleRepositoryService.vehicleList();
    }


  public List <Mechanic> getAuthMechanics () {
      List <Mechanic> mechanics = new ArrayList<>();
      Mechanic authMechanic = mechanicRepositoryService.mechanicByLogin(timeTableService.extractLogin());
      mechanics.add(authMechanic);
      return mechanics;
    }


}
