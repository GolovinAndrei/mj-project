package by.stech.web;

import by.stech.model.Customer;
import by.stech.model.Vehicle;
import by.stech.services.impl.CustomerRepositoryService;
import by.stech.services.impl.VehicleRepositoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
@RequestMapping(path = "/vehiclePanel")
public class VehicleController {

    @Autowired
    private VehicleRepositoryService vehicleRepositoryService;

    @Autowired
    private CustomerRepositoryService customerRepositoryService;

    @Autowired
    private Vehicle vehicle;


    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView get(ModelAndView modelAndView) {
        return fillVehicle(modelAndView);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ModelAndView add(ModelAndView modelAndView, @ModelAttribute("vehicle") Vehicle vehicle,  BindingResult bindingResult) {
        vehicleRepositoryService.addVehicle(vehicle);
        return fillVehicle(modelAndView);
    }

   @RequestMapping(method = RequestMethod.POST, params = "vehicleIdForDelete")
    public ModelAndView delete(ModelAndView modelAndView, @RequestParam("vehicleIdForDelete") Long vehicleIdForDelete) {
        vehicleRepositoryService.deleteVehicle(vehicleIdForDelete);
        return fillVehicle(modelAndView);
    }

   @RequestMapping(method = RequestMethod.POST, params = "vehicleIdForEdit")
    public ModelAndView edit(ModelAndView modelAndView, @RequestParam("vehicleIdForEdit") Long vehicleIdForEdit) {
        final Vehicle vehicleToEdit = vehicleRepositoryService.vehicleList().stream()
                .filter(vehicle -> vehicle.getId().equals(vehicleIdForEdit))
                .findAny()
                .orElse(vehicle);
        return fillVehicle(modelAndView, vehicleToEdit);
    }

    public ModelAndView fillVehicle(ModelAndView modelAndView) {
        return fillVehicle(modelAndView, vehicle);
    }


    public ModelAndView fillVehicle(ModelAndView modelAndView, Vehicle vehicle) {
        //modelAndView.clear();
       final List<Vehicle> vehicles = vehicleRepositoryService.vehicleList();
        modelAndView.addObject("vehicles", vehicles);
        modelAndView.addObject("vehicle", vehicle);
        return modelAndView;
    }


    @ModelAttribute ("customersForVehicle")
    public List<Customer> customersForVehicle (){
        return customerRepositoryService.customerList();
    }

}

