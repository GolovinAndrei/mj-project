package by.stech.web;

import by.stech.model.Fault;
import by.stech.model.Mechanic;
import by.stech.model.Order;
import by.stech.model.Vehicle;
import by.stech.services.impl.FaultRepositoryService;
import by.stech.services.impl.MechanicRepositoryService;
import by.stech.services.impl.OrderRepositoryService;
import by.stech.services.impl.VehicleRepositoryService;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;

@Transactional
@Controller
//@RequestMapping(path = "/orderPanel")
public class OrderController {

    @Autowired
    private OrderRepositoryService orderRepositoryService;

    @Autowired
    private VehicleRepositoryService vehicleRepositoryService;

    @Autowired
    private FaultRepositoryService faultRepositoryService;

    @Autowired
    private MechanicRepositoryService mechanicRepositoryService;

    @Autowired
    private Order order;

    @Autowired
    Container container;

    private static final String STATUS_NEW = "new";
    private static final String STATUS_COMPLETE = "complete";

    @RequestMapping(path = "/orderPanel", method= RequestMethod.GET)
    public ModelAndView get (ModelAndView modelAndView,  @RequestParam(value = "page", required = false) Integer page){
        return fillOrder(modelAndView, page, container);

    }


   @RequestMapping (path = "/orderPanel", method=RequestMethod.POST)
    public ModelAndView add (ModelAndView modelAndView, @ModelAttribute("container") Container container, @RequestParam(value = "page", required = false) Integer page,
                             @RequestParam("id") Long id){
        orderRepositoryService.add(order);
        return fillOrder(modelAndView, page);
    }

    @RequestMapping (path = "/orderPanel", method=RequestMethod.POST, params =  "pageSize")
    public ModelAndView changePagination (ModelAndView modelAndView, @RequestParam(value = "pageSize") Integer pageSize,
                                          @RequestParam(value = "page", required = false) Integer page){
        modelAndView.addObject("pageSize", pageSize);

        return fillOrder(modelAndView, page);
    }


    @RequestMapping (path = "/orderPanel", method=RequestMethod.POST, params = "orderIdForDelete")
    public ModelAndView delete (ModelAndView modelAndView, @RequestParam("orderIdForDelete") Long orderIdForDelete,
    @RequestParam(value = "page", required = false) Integer page){
        orderRepositoryService.delete(orderIdForDelete);
        return fillOrder(modelAndView, page);
    }


   @RequestMapping (path = "/orderPanel", method=RequestMethod.POST, params = "orderIdForEdit")
    public ModelAndView edit (ModelAndView modelAndView, @RequestParam ( value = "orderIdForEdit") Long orderIdForEdit,
                              @RequestParam(value = "page", required = false) Integer page){
        final Order orderToEdit = orderRepositoryService.orderList().stream()
                .filter(order -> order.getId().equals(orderIdForEdit))
                .findAny()
                .orElse(order);
        return fillOrder(modelAndView,  orderToEdit, page, container);
    }

    private ModelAndView fillOrder(ModelAndView modelAndView, Integer page) {
        return fillOrder(modelAndView, order, page, container);
    }

    private ModelAndView fillOrder(ModelAndView modelAndView, Integer page, Container container) {
        return fillOrder(modelAndView, order, page, container);
    }


    private ModelAndView fillOrder(ModelAndView modelAndView, Order order, Integer page, Container container){
        final List<Order> orders =orderRepositoryService.orderList();
        final int currentPage = page == null ? 1 : page;
        int pageSize=5;
        if (modelAndView.getModelMap().get("pageSize")!=null) {
            pageSize = (Integer) modelAndView.getModelMap().get("pageSize");
        }
        int from = (currentPage - 1) * pageSize;
        int to = Math.min(currentPage * pageSize, orders.size());
        final List<Order> currentPageOrders = orders.subList(from, to);
        final List<Integer> pagination = new ArrayList<>();
        for (int i = 0; i < orders.size(); i = i + pageSize) {
            pagination.add(i / pageSize + 1);
        }

        modelAndView.addObject("container", container);
        modelAndView.addObject("orders", orders);
        modelAndView.addObject("order", order);
        modelAndView.addObject("currentPageOrders", currentPageOrders);
        modelAndView.addObject("pagination", pagination);
        return modelAndView;
    }

    @ModelAttribute ("vehiclesForOrder")
    public List<Vehicle> vehicleList (){
        return vehicleRepositoryService.vehicleList();
    }

    @ModelAttribute ("faultsForOrder")
    public List<Fault> faultList (){
        return faultRepositoryService.faultList();
    }

    @ModelAttribute ("orderStatuses")
    public List<String> orderStatuses (){
        List <String> statuses= new ArrayList<>();
        statuses.add(STATUS_NEW);
        statuses.add(STATUS_COMPLETE);
        return statuses;
    }

    @ModelAttribute ("mechanicsForOrder")
    public List<Mechanic> mechanicsForOrder (){
        return mechanicRepositoryService.mechanicList();
    }



}




