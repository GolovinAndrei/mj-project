package by.stech.web;

import by.stech.model.Discount;
import by.stech.services.impl.DiscountRepositoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
@RequestMapping(path = "/discountPanel")
public class DiscountController {

    @Autowired
    private DiscountRepositoryService discountRepositoryService;

    @Autowired
    private Discount discount;

    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView get(ModelAndView modelAndView) {
        return fillDiscount(modelAndView);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ModelAndView add(ModelAndView modelAndView, @ModelAttribute("discount") Discount discount) {
        discountRepositoryService.addDiscount(discount);
        return fillDiscount(modelAndView);
    }

    @RequestMapping(method = RequestMethod.POST, params = "discountIdForDelete")
    public ModelAndView delete(ModelAndView modelAndView, @RequestParam("discountIdForDelete") Long discountIdForDelete) {
        discountRepositoryService.deleteDiscount(discountIdForDelete);
        return fillDiscount(modelAndView);
    }

    @RequestMapping(method = RequestMethod.POST, params = "discountIdForEdit")
    public ModelAndView edit(ModelAndView modelAndView, @RequestParam("discountIdForEdit") Long discountIdForEdit) {
        final Discount discountToEdit = discountRepositoryService.discountList().stream()
                .filter(discount -> discount.getId().equals(discountIdForEdit))
                .findAny()
                .orElse(discount);
        return fillDiscount(modelAndView, discountToEdit);
    }

    public ModelAndView fillDiscount(ModelAndView modelAndView) {
        return fillDiscount(modelAndView, discount);
    }


    public ModelAndView fillDiscount(ModelAndView modelAndView, Discount discount) {
        final List<Discount> discounts = discountRepositoryService.discountList();
        modelAndView.addObject("discounts", discounts);
        modelAndView.addObject("discount", discount);
        return modelAndView;
    }

}


