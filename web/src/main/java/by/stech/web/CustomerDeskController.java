package by.stech.web;

import by.stech.model.Customer;
import by.stech.model.Vehicle;
import by.stech.services.impl.CustomerDeskService;
import by.stech.services.impl.CustomerRepositoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
public class CustomerDeskController {

    @Autowired
    CustomerDeskService customerDeskService;

    @Autowired
    CustomerRepositoryService customerRepositoryService;

    @RequestMapping(path = "/customersHistory", method= RequestMethod.GET)
    public ModelAndView getHistory (ModelAndView modelAndView){
        List <Vehicle> vehicles = customerDeskService.customersVehicles();

        Customer customer = customerRepositoryService.findCustomerByLogin(customerDeskService.extractLogin());
        String name = customer.getName();
        String surname = customer.getSurname();
        String customersStatus = customer.getDiscount().getStatus();
        int customersDiscount = customer.getDiscount().getDiscount();
        modelAndView.addObject("name", name);
        modelAndView.addObject("surname", surname);
        modelAndView.addObject("vehicles",vehicles);
        modelAndView.addObject("customersStatus",customersStatus);
        modelAndView.addObject("customersDiscount",customersDiscount);
        return modelAndView;
    }

    @RequestMapping(path = "/customersDesk", method= RequestMethod.GET)
    public ModelAndView getCurrentOrders (ModelAndView modelAndView){
        return modelAndView.addObject ("orders", customerDeskService.customersOrdersInWork());
    }

}
