
package by.stech.web;

import by.stech.model.Mechanic;
import by.stech.services.impl.MechanicRepositoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
@RequestMapping(path = "/employeePanel")
public class EmployeeController {

    @Autowired
   private MechanicRepositoryService mechanicRepositoryService;

   @Autowired
    private Mechanic mechanic;


    @RequestMapping (method=RequestMethod.GET)
    public ModelAndView get (ModelAndView modelAndView){
        return fillEmployee(modelAndView);
    }

    @RequestMapping (method=RequestMethod.POST  )
    public ModelAndView add (ModelAndView modelAndView, @ModelAttribute("mechanic")  Mechanic mechanic){
        mechanicRepositoryService.add(mechanic);
        return fillEmployee(modelAndView);
    }

    @RequestMapping (method=RequestMethod.POST, params = "mechanicIdForDelete")
    public ModelAndView delete (ModelAndView modelAndView, @RequestParam ("mechanicIdForDelete") Long mechanicIdForDelete){
        mechanicRepositoryService.delete(mechanicIdForDelete);
        return fillEmployee(modelAndView);
    }

    @RequestMapping (method=RequestMethod.POST, params = "mechanicIdForEdit")
    public ModelAndView edit (ModelAndView modelAndView, @RequestParam ("mechanicIdForEdit") Long mechanicIdForEdit){
        final Mechanic mechanicToEdit = mechanicRepositoryService.mechanicList().stream()
                .filter(mechanic -> mechanic.getId().equals(mechanicIdForEdit))
                .findAny()
                .orElse(mechanic);
        return fillEmployee(modelAndView,  mechanicToEdit);
    }

    public ModelAndView fillEmployee(ModelAndView modelAndView) {
        return fillEmployee(modelAndView, mechanic);
    }


    public ModelAndView fillEmployee (ModelAndView modelAndView, Mechanic mechanic){
        final List<Mechanic> mechanics =mechanicRepositoryService.mechanicList();
        modelAndView.addObject("mechanics", mechanics);
        modelAndView.addObject("mechanic", mechanic);
        return modelAndView;
    }

}


