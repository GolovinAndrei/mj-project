package by.stech.web;

import by.stech.model.Customer;
import by.stech.model.Discount;
import by.stech.services.impl.CustomerRepositoryService;
import by.stech.services.impl.DiscountRepositoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;


@Controller
@RequestMapping(path = "/customerPanel")
public class CustomerController {

    @Autowired
    private CustomerRepositoryService customerRepositoryService;
    @Autowired
    private DiscountRepositoryService discountRepositoryService;


   @Autowired
    private Customer customer;

    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView get(ModelAndView modelAndView) {
        return fillCustomer(modelAndView);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ModelAndView add(ModelAndView modelAndView, @ModelAttribute("customer") Customer customer) {
        customerRepositoryService.addCustomer(customer);
        return fillCustomer(modelAndView);
    }

    @RequestMapping(method = RequestMethod.POST, params = "customerIdForDelete")
    public ModelAndView delete(ModelAndView modelAndView, @RequestParam("customerIdForDelete") Long customerIdForDelete) {
        customerRepositoryService.deleteCustomer(customerIdForDelete);
        return fillCustomer(modelAndView);
    }

    @RequestMapping(method = RequestMethod.POST, params = "customerIdForEdit")
    public ModelAndView edit(ModelAndView modelAndView, @RequestParam("customerIdForEdit") Long customerIdForEdit) {
        final Customer customerToEdit = customerRepositoryService.customerList().stream()
                .filter(customer -> customer.getId().equals(customerIdForEdit))
                .findAny()
                .orElse(customer);
        return fillCustomer(modelAndView, customerToEdit);
    }

    public ModelAndView fillCustomer(ModelAndView modelAndView) {
        return fillCustomer(modelAndView, customer);
    }


    public ModelAndView fillCustomer(ModelAndView modelAndView, Customer customer) {
        final List<Customer> customers = customerRepositoryService.customerList();
        modelAndView.addObject("customers", customers);
        modelAndView.addObject("customer", customer);
        return modelAndView;
    }

    @ModelAttribute ("discountForCustomer")
    public List<Discount> discountList (){
       return discountRepositoryService.discountList();
    }


}


