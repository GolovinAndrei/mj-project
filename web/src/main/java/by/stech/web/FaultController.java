package by.stech.web;

import by.stech.model.Fault;
import by.stech.services.impl.FaultRepositoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
@RequestMapping(path = "/faultPanel")
public class FaultController {

    @Autowired
    private FaultRepositoryService faultRepositoryService;

    @Autowired
    private Fault fault;


    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView get(ModelAndView modelAndView) {
        return fillFault(modelAndView);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ModelAndView add(ModelAndView modelAndView, @ModelAttribute("fault") Fault fault) {
        faultRepositoryService.addFault(fault);
        return fillFault(modelAndView);
    }

    @RequestMapping(method = RequestMethod.POST, params = "faultIdForDelete")
    public ModelAndView delete(ModelAndView modelAndView, @RequestParam("faultIdForDelete") Long faultIdForDelete) {
        faultRepositoryService.deleteFault(faultIdForDelete);
        return fillFault(modelAndView);
    }

    @RequestMapping(method = RequestMethod.POST, params = "faultIdForEdit")
    public ModelAndView edit(ModelAndView modelAndView, @RequestParam("faultIdForEdit") Long faultIdForEdit) {
        final Fault faultToEdit = faultRepositoryService.faultList().stream()
                .filter(fault -> fault.getId().equals(faultIdForEdit))
                .findAny()
                .orElse(fault);
        return fillFault(modelAndView, faultToEdit);
    }

    public ModelAndView fillFault(ModelAndView modelAndView) {
        return fillFault(modelAndView, fault);
    }


    public ModelAndView fillFault(ModelAndView modelAndView, Fault fault) {
        final List<Fault> faults = faultRepositoryService.faultList();
        modelAndView.addObject("faults", faults);
        modelAndView.addObject("fault", fault);
        return modelAndView;
    }

}

