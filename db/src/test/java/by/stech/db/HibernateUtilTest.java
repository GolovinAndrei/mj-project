package by.stech.db;

import by.stech.db.connection.HibernateUtil;
import by.stech.model.Customer;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.Test;

import javax.persistence.EntityManager;

public class HibernateUtilTest {



    @Test
    public void initHibernate2() {

        final SessionFactory sessionFactory =HibernateUtil.getSessionFactory();

        final Session session = sessionFactory.openSession();

        sessionFactory.close();
    }

}


