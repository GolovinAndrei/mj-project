package by.stech.db.connection;

import by.stech.model.Customer;
import by.stech.model.Vehicle;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.sql.Date;
import java.sql.SQLException;


public interface VehicleDao extends CommonDao<Vehicle> {

    Vehicle getByNextService (Date date) throws SQLException, IOException;
    Vehicle getByOwner (Customer customer) throws SQLException, IOException;
    Vehicle getByRegNumber (String regNumber) throws SQLException, IOException;
}
