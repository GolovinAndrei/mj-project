package by.stech.db.connection;

import by.stech.model.Employee;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;


public interface EmployeeDao extends CommonDao<Employee> {

    Employee getByLogin (String login) throws SQLException, IOException;
    Employee getBySalaryLevel (long lower, long upper) throws  SQLException;
    public List<String> getAllLogins() throws SQLException, IOException;
}
