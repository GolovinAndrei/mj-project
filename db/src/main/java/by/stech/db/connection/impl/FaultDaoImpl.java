package by.stech.db.connection.impl;

import by.stech.db.connection.FaultDao;
import by.stech.model.Fault;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;


@Repository
public class FaultDaoImpl implements FaultDao {

    private SessionFactory sessionFactory;

    @Autowired
    public FaultDaoImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Fault get(Long id) throws SQLException, IOException {
        return null;
    }

    @Override
    public List<Fault> getAll() throws SQLException, IOException {
        return null;
    }

    @Override
    public void update(Fault item) throws SQLException, IOException {

    }

    @Override
    public void create(Fault item) throws SQLException, IOException {

    }

    @Override
    public void delete(Long id) throws SQLException, IOException {

    }
}

