package by.stech.db.connection;

import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public interface CommonDao<T> {

    T get(Long id) throws SQLException, IOException;

   List<T> getAll() throws SQLException, IOException;

   void update (T item) throws SQLException, IOException;

    void create (T item) throws SQLException, IOException;

   void delete (Long id) throws SQLException, IOException;
}
