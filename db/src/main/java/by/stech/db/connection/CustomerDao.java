package by.stech.db.connection;

import by.stech.model.Customer;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;


public interface CustomerDao extends CommonDao<Customer> {
    public Customer getByLogin (String login) throws SQLException, IOException;

    public List<String> getAllLogins() throws SQLException, IOException;
}
