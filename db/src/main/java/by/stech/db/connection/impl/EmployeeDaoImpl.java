package by.stech.db.connection.impl;

import by.stech.db.connection.CustomerDao;
import by.stech.db.connection.EmployeeDao;
import by.stech.db.connection.HibernateUtil;
import by.stech.db.connection.JdbcConnector;
import by.stech.model.Customer;
import by.stech.model.Employee;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.NativeQuery;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;


@Repository
public class EmployeeDaoImpl implements EmployeeDao {

    private static EmployeeDao employeeDao;

    private SessionFactory sessionFactory = HibernateUtil.getSessionFactory();

    public static EmployeeDao getInstance() {
        if (employeeDao == null) {
            synchronized (EmployeeDaoImpl.class) {
                if (employeeDao == null) {
                    employeeDao = new EmployeeDaoImpl();
                }
            }
        }

        return employeeDao;
    }

    @Override
    public Employee getByLogin(String login) throws SQLException, IOException {

            Connection con = JdbcConnector.getConnection();
            PreparedStatement ps = con.prepareStatement("SELECT * FROM employees WHERE login=?");
            ps.setString(1, login);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return setEmployee(rs);
            } else {
                return null;
            }
        }

    @Override
    public Employee getBySalaryLevel(long lower, long upper) throws SQLException {
        return null;
    }

    @Override
    public Employee get(Long id) throws SQLException {
        try (final Session session = sessionFactory.openSession()){
            return session.get(Employee.class, id);
        }

    }

    @Override
    public ArrayList<Employee> getAll() throws SQLException, IOException {
        Connection connection = JdbcConnector.getConnection();
        Statement s = connection.createStatement();
        ResultSet rs = s.executeQuery("SELECT * FROM employees");
        ArrayList<Employee> employeeList = new ArrayList<>();
        while (rs.next()) {
            final Employee employee = setEmployee(rs);
            employeeList.add(employee);
        }
        return employeeList;

    }


    @Override
    public void update(Employee item) throws SQLException {

    }

    @Override
    public void create(Employee item) throws SQLException, IOException {
       Connection connection = JdbcConnector.getConnection();
        PreparedStatement ps = connection.prepareStatement("INSERT INTO employees ( id, name, surname, password, address, phone, login) VALUES" +
                "(?,?,?,?,?,?,?)");
        fillNewCustomer(item, ps);
        ps.executeUpdate();

    }



    @Override
    public void delete(Long id) throws SQLException {
        try (final Session session=sessionFactory.openSession()){
            session.getTransaction().begin();
            session.delete(session.get(Employee.class, id));
            session.getTransaction().commit();
        }
    }

    @Override
    public List<String> getAllLogins() throws SQLException, IOException {
        Connection con = JdbcConnector.getConnection();
        Statement s = con.createStatement();
        ResultSet rs = s.executeQuery("SELECT login FROM employees");
        ArrayList<String> employeesLogins = new ArrayList<>();
        while (rs.next()) {
            employeesLogins.add(rs.getString("login"));
        }
        return employeesLogins;
    }

    private Employee setEmployee (ResultSet rs) throws SQLException {
        final Employee employee = new Employee();
        employee.setId(rs.getLong("id"));
        employee.setName(rs.getString("name"));
        employee.setSurname(rs.getString("surname"));
        employee.setPassword(rs.getString("password"));
        employee.setAddress(rs.getString("Address"));
        employee.setPhone(rs.getString("phone"));
        employee.setLogin(rs.getString("login"));
        return employee;
    }

    private void fillNewCustomer(Employee employee, PreparedStatement ps) throws SQLException {
        ps.setLong(1, employee.getId());
        ps.setString(2, employee.getName());
        ps.setString(3, employee.getSurname());
        ps.setString(4, employee.getPassword());
        ps.setString(5, employee.getAddress());
        ps.setString(6, employee.getPhone());
        ps.setString(7, employee.getLogin());

    }
}

