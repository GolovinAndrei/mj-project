package by.stech.db.connection;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class HibernateUtil {

    private static EntityManagerFactory emf;

    private static SessionFactory sessionFactory;

    public static EntityManager getEntityManager() {
        if (emf == null) {
            emf = Persistence.createEntityManagerFactory("test");
        }
        return emf.createEntityManager();
    }

    public static void close() {
        emf.close();
    }

    public static SessionFactory getSessionFactory (){
        if (sessionFactory==null){
            synchronized (HibernateUtil.class){
                if (sessionFactory==null){
                    sessionFactory = new Configuration().configure().buildSessionFactory();
                }
            }
        }
        return sessionFactory;
    }

    public static void closeSf (){
        if (sessionFactory!=null){
            sessionFactory.close();
        }
    }
}
