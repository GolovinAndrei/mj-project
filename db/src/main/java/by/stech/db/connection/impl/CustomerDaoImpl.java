package by.stech.db.connection.impl;

import by.stech.db.connection.CustomerDao;
import by.stech.db.connection.HibernateUtil;
import by.stech.db.connection.JdbcConnector;
import by.stech.model.Customer;
import by.stech.model.Discount;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;


@Repository
public class CustomerDaoImpl implements CustomerDao {

    private static CustomerDao customerDao;

   private SessionFactory sessionFactory = HibernateUtil.getSessionFactory();

    public CustomerDaoImpl() {
    }

    public static CustomerDao getInstance() {
        if (customerDao == null) {
            synchronized (CustomerDaoImpl.class) {
                if (customerDao == null) {
                    customerDao = new CustomerDaoImpl();
                }
            }
        }

        return customerDao;
    }


    private Customer setCustomer (ResultSet rs) throws SQLException {
        final Customer customer = new Customer();
        customer.setId(rs.getLong("id"));
        customer.setName(rs.getString("name"));
        customer.setPassword(rs.getString("password"));
        customer.setLogin(rs.getString("login"));
        customer.setEmail(rs.getString("email"));
        customer.setSurname(rs.getString("surname"));
        customer.setDiscount((Discount) rs.getObject("discount"));

        return customer;
    }

    @Override
    public Customer getByLogin(String login) throws SQLException, IOException {
        Connection con = JdbcConnector.getConnection();
        PreparedStatement ps = con.prepareStatement("SELECT * FROM customers WHERE login=?");
        ps.setString(1, login);
        ResultSet rs = ps.executeQuery();
        if (rs.next()) {
            return setCustomer(rs);
        } else {
            return null;
        }
    }




        @Override
    public Customer get(Long id) throws SQLException, IOException {
        Connection con = JdbcConnector.getConnection();
        PreparedStatement ps = con.prepareStatement("SELECT * FROM customers WHERE id=?");
        ps.setLong(1, id);
        ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return setCustomer(rs);
            } else {
                return null;
            }

        }


    @Override
    public List<Customer> getAll() throws SQLException, IOException {
            Connection connection = JdbcConnector.getConnection();
            Statement s = connection.createStatement();
                ResultSet rs = s.executeQuery("SELECT * FROM customers");
                    List<Customer> customerList = new ArrayList<Customer>();
                    while (rs.next()) {
                        final Customer customer = setCustomer(rs);
                        customerList.add(customer);
                    }
                    return customerList;

                }

    @Override
    public void update (Customer item) throws SQLException, IOException {
        Connection connection = JdbcConnector.getConnection();
        PreparedStatement ps = connection.prepareStatement(
                    "UPDATE customers SET name = ?, surname=?, password = ?, discount= ?, email=? WHERE id = ?");
                fillNewCustomer(item, ps);
                ps.setLong(6, item.getId());
                ps.executeUpdate();
            }

    private void fillNewCustomer(Customer customer, PreparedStatement ps) throws SQLException {
        ps.setLong(1, customer.getId());
        ps.setString(2, customer.getName());
        ps.setString(3, customer.getSurname());
        ps.setString(4, customer.getPassword());
        ps.setString(5, customer.getLogin());
        ps.setObject(6, customer.getDiscount());
        ps.setString(7, customer.getEmail());

    }

    @Override
    public void create(Customer item) throws SQLException, IOException {
        Connection connection = JdbcConnector.getConnection();
            PreparedStatement ps = connection.prepareStatement("INSERT INTO customers (login, name, surname,  discount, password, email) VALUES" +
                    "(?,?,?,?,?,?,?)");
                fillNewCustomer(item, ps);
                ps.executeUpdate();
            }


    @Override
    public void delete(Long id) throws SQLException, IOException {
        Connection connection = JdbcConnector.getConnection();
            PreparedStatement ps = connection.prepareStatement("DELETE FROM customers WHERE id=?");
                ps.setLong(1, id);
                ps.executeUpdate();
            }

    @Override
    public List<String> getAllLogins() throws SQLException, IOException {
        Connection con = JdbcConnector.getConnection();
        Statement s = con.createStatement();
        ResultSet rs = s.executeQuery("SELECT login FROM customers");
        ArrayList<String> customersLogins = new ArrayList<>();
            while (rs.next()) {
                customersLogins.add(rs.getString("login"));
                    }
            return customersLogins;
    }



}

