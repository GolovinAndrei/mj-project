package by.stech.db.connection.impl;

import by.stech.db.connection.JdbcConnector;
import by.stech.db.connection.VehicleDao;
import by.stech.model.Customer;
import by.stech.model.Vehicle;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;


@Repository
public class VehicleDaoImpl implements VehicleDao {


private SessionFactory sessionFactory;

   @Autowired
   public VehicleDaoImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }


   private static VehicleDao vehicleDAO;

    private VehicleDaoImpl() {
    }

    public static VehicleDao getInstance() {
        if (vehicleDAO == null) {
            synchronized (CustomerDaoImpl.class) {
                if (vehicleDAO == null) {
                    vehicleDAO = new VehicleDaoImpl();
                }
            }
        }

        return vehicleDAO;
    }


    private Vehicle setVehicle (ResultSet rs) throws SQLException {
        final Vehicle vehicle = new Vehicle();
        vehicle.setId(rs.getLong("id"));
        vehicle.setBrand(rs.getString("brand"));
        vehicle.setModel(rs.getString("model"));
        vehicle.setYear(rs.getInt("Year"));
        vehicle.setRegNumber(rs.getString("regNumber"));
        vehicle.setNextServiceDate(rs.getDate("nextServiceData"));
        vehicle.setCustomer((Customer) rs.getObject("customer"));
        return vehicle;
    }

    @Override
    public Vehicle getByNextService(Date date) throws SQLException, IOException {
        Connection con = JdbcConnector.getConnection();
            PreparedStatement ps = con.prepareStatement("SELECT * FROM vehicles WHERE nextServiceDate=?");
                ps.setDate(1, date);
                ResultSet rs = ps.executeQuery();
                    if (rs.next()) {
                        return setVehicle(rs);
                    } else {
                        return null;
                    }

    }


    @Override
    public Vehicle getByOwner(Customer customer) throws SQLException, IOException {
        Connection con = JdbcConnector.getConnection();
            PreparedStatement ps = con.prepareStatement("SELECT * FROM vehicles WHERE customer=?");
                ps.setLong(1, customer.getId());
                ResultSet rs = ps.executeQuery();
                    if (rs.next()) {
                        return setVehicle(rs);
                    } else {
                        return null;
                    }

    }


    @Override
    public Vehicle getByRegNumber(String regNumber) throws SQLException, IOException {
        Connection con = JdbcConnector.getConnection();
            PreparedStatement ps = con.prepareStatement("SELECT * FROM vehicles WHERE regNumber=?");
                ps.setString(1, regNumber);
                ResultSet rs = ps.executeQuery();
                    if (rs.next()) {
                        return setVehicle(rs);
                    } else {
                        return null;
                    }

    }

    @Override
    public Vehicle get(Long id) throws SQLException, IOException {
        Connection con = JdbcConnector.getConnection();
            PreparedStatement ps = con.prepareStatement("SELECT * FROM vehicles WHERE id=?");
                ps.setLong(1, id);
                ResultSet rs = ps.executeQuery();
                    if (rs.next()) {
                        return setVehicle(rs);
                    } else {
                        return null;
                    }

    }

    @Override
    public ArrayList<Vehicle> getAll() throws SQLException, IOException {
        Connection connection = JdbcConnector.getConnection();
            Statement s = connection.createStatement();
                ResultSet rs = s.executeQuery("SELECT * FROM vehicles");
                    ArrayList<Vehicle> vehicleList = new ArrayList<Vehicle>();
                    while (rs.next()) {
                        final Vehicle vehicle = setVehicle(rs);
                        vehicleList.add(vehicle);
                    }
                    return vehicleList;

    }

    private void fillNewVehicle(Vehicle vehicle, PreparedStatement ps) throws SQLException {
        ps.setLong(1, vehicle.getId());
        ps.setString(2, vehicle.getModel());
        ps.setString(3, vehicle.getBrand());
        ps.setInt(4, vehicle.getYear());
        ps.setString(5, vehicle.getRegNumber());
        ps.setDate(6, (Date) vehicle.getNextServiceDate());
        ps.setObject(7, vehicle.getCustomer());

    }

    @Override
    public void update(Vehicle item) throws SQLException, IOException {
        Connection connection = JdbcConnector.getConnection();
            PreparedStatement ps = connection.prepareStatement("UPDATE vehicles SET model = ?, brand=?, Year = ?, regNumber = ?, nextServiceDate= ?, customer=? WHERE id = ?");
                fillNewVehicle(item, ps);
                ps.setLong(7, item.getId());
                ps.executeUpdate();

    }

    @Override
    public void create(Vehicle item) throws SQLException, IOException {
        Connection connection = JdbcConnector.getConnection();
            PreparedStatement ps = connection.prepareStatement("INSERT INTO vehicles (model, brand, Year, regNumber, nextServiceDate, customer) VALUES" +
                    "(?,?,?,?,?,?)");
                fillNewVehicle(item, ps);
                ps.executeUpdate();

    }

    @Override
    public void delete(Long id) throws SQLException, IOException {
        Connection connection = JdbcConnector.getConnection();
            PreparedStatement ps = connection.prepareStatement("DELETE FROM vehicles WHERE id=?");
                ps.setLong(1, id);
                ps.executeUpdate();

    }
}

