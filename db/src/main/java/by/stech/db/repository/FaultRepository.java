package by.stech.db.repository;

import by.stech.model.Fault;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface FaultRepository extends CrudRepository<Fault, Long> {

    @Override
    List<Fault> findAll();

    @Override
    Optional<Fault> findById (Long id);
}
