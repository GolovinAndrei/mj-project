package by.stech.db.repository;

import by.stech.model.Mechanic;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import java.util.List;
import java.util.Optional;

@Repository
public interface MechanicRepository extends CrudRepository<Mechanic, Long> {

    @Override
    List<Mechanic> findAll();

   Optional <Mechanic> findAllByLogin (String login);


}