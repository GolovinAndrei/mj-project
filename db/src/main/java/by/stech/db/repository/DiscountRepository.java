package by.stech.db.repository;


import by.stech.model.Discount;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DiscountRepository extends CrudRepository<Discount, Long> {

    @Override
    List<Discount> findAll();

  }
