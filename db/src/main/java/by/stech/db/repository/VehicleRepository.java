package by.stech.db.repository;

import by.stech.model.Customer;
import by.stech.model.Vehicle;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
public interface VehicleRepository extends CrudRepository<Vehicle, Long> {

    @Override
    List<Vehicle> findAll();

    Optional<Vehicle> findAllByNextServiceDate (Date date);

    List <Vehicle> findAllByCustomer (Customer customer);

    Vehicle findByRegNumber(String regNumber);


}
