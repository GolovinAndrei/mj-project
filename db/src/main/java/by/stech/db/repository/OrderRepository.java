package by.stech.db.repository;

import by.stech.model.Order;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderRepository extends  CrudRepository<Order, Long> {

   // Sort sortByOderStatus = new Sort (Sort.Direction.DESC, "Order.orderStatus");

    @Override
    List<Order> findAll();


}
