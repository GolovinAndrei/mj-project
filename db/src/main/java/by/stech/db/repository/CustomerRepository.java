package by.stech.db.repository;

import by.stech.model.Customer;
import org.springframework.stereotype.Repository;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CustomerRepository extends CrudRepository <Customer, Long> {

    @Override
    List<Customer> findAll();

    Optional<Customer> findAllByLogin (String login);

    List <String> findAllByLoginNotNull ();

}
