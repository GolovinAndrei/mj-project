/*
package by.stech.db.SpingDaoImpl;

import by.stech.db.connection.VehicleDao;
import by.stech.db.repository.VehicleRepository;
import by.stech.model.Customer;
import by.stech.model.Vehicle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.sql.Date;
import java.sql.SQLException;
import java.util.List;

@Service
public class VehicleRepositoryImpl implements VehicleDao {

    @Autowired
    private VehicleRepository vehicleRepository;

    @Override
    public Vehicle getByNextService(Date date) throws SQLException, IOException {
        return vehicleRepository.findAllByNextServiceDate(date).orElse(null);
    }

    @Override
    public Vehicle getByOwner(Customer customer) throws SQLException, IOException {
        return vehicleRepository.findAllByCustomer(customer).orElse(null);
    }

    @Override
    public Vehicle getByRegNumber(String regNumber) throws SQLException, IOException {
        return vehicleRepository.findByRegNumber(regNumber);
    }

    @Override
    public Vehicle get(Long id) throws SQLException, IOException {
        return vehicleRepository.findById(id).orElse(null);
    }

    @Override
    public List<Vehicle> getAll() throws SQLException, IOException {
        return vehicleRepository.findAll();
    }

    @Override
    public void update(Vehicle item) throws SQLException, IOException {
        vehicleRepository.save(item);
    }

    @Override
    public void create(Vehicle item) throws SQLException, IOException {
        vehicleRepository.save(item);
    }

    @Override
    public void delete(Long id) throws SQLException, IOException {
        vehicleRepository.deleteById(id);
    }
}
*/
