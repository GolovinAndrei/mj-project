package by.stech.db.SpingDaoImpl;

import by.stech.db.connection.CustomerDao;
import by.stech.db.repository.CustomerRepository;
import by.stech.model.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;


@Service
public class CustomerRepositoryImpl implements CustomerDao {

    @Autowired
    private CustomerRepository customerRepository;

    @Override
    public Customer getByLogin(String login) throws SQLException, IOException {
        return customerRepository.findAllByLogin(login).orElse(null);
    }


    @Override
    public List<String> getAllLogins() throws SQLException, IOException {
        return customerRepository.findAllByLoginNotNull();
    }

    @Override
    public Customer get(Long id) throws SQLException, IOException {
        return customerRepository.findById(id).orElse(null);
    }

    @Override
    public List<Customer> getAll() throws SQLException, IOException {
        return customerRepository.findAll();
    }

    @Override
    public void update(Customer item) throws SQLException, IOException {
        customerRepository.save(item);
    }

    @Override
    public void create(Customer item) throws SQLException, IOException {
        customerRepository.save(item);
    }

    @Override
    public void delete(Long id) throws SQLException, IOException {
        customerRepository.deleteById(id);
    }
}