package by.stech.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;


@Entity
@Table(name = "faults")
public class Fault {

    @Id
       /* @GenericGenerator(name = "one-one", strategy = "foreign", parameters = @Parameter(name = "property", value = "order")
    )
    @GeneratedValue (generator = "one-one")*/
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "fault_id")
    private Long id;
    @Column
    private String faultDescription;
    @Column
    private String sparePart;
    @Column
    private long repairCost;

    @OneToMany(mappedBy = "fault", fetch = FetchType.EAGER)
    private List<Order> orders = new ArrayList<>();

    public Fault() {
    }

    public List<Order> getOrders() {
        return orders;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFaultDescription() {
        return faultDescription;
    }

    public void setFaultDescription(String faultDescription) {
        this.faultDescription = faultDescription;
    }

    public String getSparePart() {
        return sparePart;
    }

    public void setSparePart(String sparePart) {
        this.sparePart = sparePart;
    }

    public long getRepairCost() {
        return repairCost;
    }

    public void setRepairCost(long repairCost) {
        this.repairCost = repairCost;
    }


}
