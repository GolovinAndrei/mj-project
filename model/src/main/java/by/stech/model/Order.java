package by.stech.model;

import javax.persistence.*;
import java.sql.Time;
import java.util.ArrayList;
import java.util.List;


@Entity
@Table (name = "orders")
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column
    private String takeCar;
    @Column
    private String finishRepair;

    @ManyToOne (cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn (name = "fault_id")
    private Fault fault;

    @ManyToOne (cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn (name = "vehicle")
    private Vehicle vehicle;

    @Column (precision = 2)
    private double totalPrice;

    @Column
    private String ordersStatus;

    @ManyToMany (cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH}, fetch = FetchType.EAGER)
    @JoinTable (name = "mechanic_order", joinColumns ={ @JoinColumn (name = "order_id")}, inverseJoinColumns = {@JoinColumn(name = "mechanic_id")})
    private List<Mechanic> mechanics = new ArrayList<>();



    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTakeCar() {
        return takeCar;
    }

    public void setTakeCar(String takeCar) {
        this.takeCar = takeCar;
    }

    public String getFinishRepair() {
        return finishRepair;
    }

    public void setFinishRepair(String finishRepair) {
        this.finishRepair = finishRepair;
    }

    public Fault getFault() {
        return fault;
    }

    public void setFault(Fault fault) {
        this.fault = fault;
    }

    public Vehicle getVehicle() {
        return vehicle;
    }

    public void setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getOrdersStatus() {
        return ordersStatus;
    }

    public void setOrdersStatus(String ordersStatus) {
        this.ordersStatus = ordersStatus;
    }


    public List<Mechanic> getMechanics() {
        return mechanics;
    }

    public void setMechanics(List<Mechanic> mechanics) {
        this.mechanics = mechanics;
    }
}
