package by.stech.model;


import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@DiscriminatorValue("m")
public class Mechanic extends Employee {

   /*@Id
    @GenericGenerator( name = "one-one", strategy = "foreign", parameters = @Parameter(name = "property", value = "employee"))
    @GeneratedValue(generator = "one-one")
    @Column(name = "employee_id")
    private Long id;

    @OneToOne (fetch = FetchType.LAZY)
    @PrimaryKeyJoinColumn (name = "employee_id")
    private Employee employee;*/

   /* @Column
    private Order order;*/

    @Column (name = "salary_for_hour", precision = 2)
    private double salaryForHour;

    @ManyToMany (cascade = CascadeType.ALL, mappedBy = "mechanics", fetch = FetchType.EAGER)
    private List<Order> orders = new ArrayList<>();

    public Mechanic() {
    }


   /* public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }*/

    /*public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }*/

    public double getSalaryForHour() {
        return salaryForHour;
    }

    public void setSalaryForHour(double salaryForHour) {
        this.salaryForHour = salaryForHour;
    }

    public List<Order> getOrders() {
        return orders;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }
}
